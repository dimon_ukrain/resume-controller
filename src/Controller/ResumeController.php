<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\Resume;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ResumeController extends AbstractController
{
    /**
     * @Route("/resume/list", name="resume_list")
     */
    public function list()
    {
        $resumes = $this->getDoctrine()->getRepository(Resume::class)->findAll();

        return $this->render('resume/list.html.twig', [
            'resumes' => $resumes,
        ]);
    }

    /**
     * @Route("/resume/new/{companyId}", name="new_resume", requirements={"companyId"="\d+"})
     */
    public function new(Request $request, $companyId)
    {
        $resume = new Resume();

        $form = $this->createFormBuilder($resume)
            ->add('jobTitle', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('resumeText', TextAreaType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label'=> 'Create','attr'=>array('class'=>'btn btn-primary mt-3')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $resume = $form->getData();
            $resume->setStartSettings();
            $resume->setCompanyId($companyId);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($resume);
            $entityManager->flush();

            return $this->redirectToRoute('resume_list');
        }

        return $this->render('resume/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/resume/edit/{id}", name="edit_resume", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $id)
    {
        $resume = new Resume();
        $resume = $this->getDoctrine()->getRepository(Resume::class)->find($id);

        $form = $this->createFormBuilder($resume)
            ->add('jobTitle', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('resumeText', TextAreaType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label'=> 'Update','attr'=>array('class'=>'btn btn-primary mt-3')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $resume->setChangedAt(new \DateTime());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('resume_list');
        }

        return $this->render('resume/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/resume/{id}", name="resume_show", requirements={"id"="\d+"})
     */
    public function show($id)
    {
        $resume = $this->getDoctrine()->getRepository(Resume::class)->find($id);

        return $this->render('resume/show.html.twig', [
            'resume' => $resume,
        ]);
    }

    /**
     * @Route("/resume/delete/{id}", name="resume_delete", requirements={"id"="\d+"}, methods={"DELETE"})
     */
    public function delete(Request $request, $id)
    {
        $resume = $this->getDoctrine()->getRepository(Resume::class)->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($resume);
        $entityManager->flush();

        $response = new Response();
        $response->send();
    }

    /**
     * @Route("/resume/like/{resumeId}", name="like_resume", requirements={"resumeId"="\d+"})
     */
    public function like(Request $request, $resumeId)
    {
        $resume = new Resume();
        $resume = $this->getDoctrine()->getRepository(Resume::class)->find($resumeId);

        $resume->setLikes($resume->getLikes() + 1);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return new JsonResponse($resume->getLikes());
    }

    /**
     * @Route("/resume/dislike/{resumeId}", name="dislike_resume", requirements={"resumeId"="\d+"})
     */
    public function dislike(Request $request, $resumeId)
    {
        $resume = new Resume();
        $resume = $this->getDoctrine()->getRepository(Resume::class)->find($resumeId);

        $resume->setDislikes($resume->getDislikes() + 1);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();

        return new JsonResponse($resume->getDislikes());
    }

    /**
     * @Route("/resume/filter/likes")
     */
    public function filterByLikes(Request $request)
    {
        $resumes = $this->getDoctrine()->getRepository(Resume::class)->findBy([],['likes' => 'DESC']);

        return $this->render('resume/__list.html.twig', [
            'resumes' => $resumes,
        ]);
    }
}
