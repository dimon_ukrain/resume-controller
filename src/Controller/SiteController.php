<?php


namespace App\Controller;

use App\Entity\Company;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SiteController extends AbstractController
{
 /**
  * @Route("/")
  */
    public function index()
    {
        return $this->render('site/main.html.twig');
    }

}