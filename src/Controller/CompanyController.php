<?php

namespace App\Controller;

use App\Entity\Company;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CompanyController extends AbstractController
{
    /**
     * @Route("/company/list", name="company_list")
     */
    public function list()
    {
        $companies = $this->getDoctrine()->getRepository(Company::class)->findAll();

        return $this->render('company/list.html.twig', [
            'companies' => $companies,
        ]);
    }

    /**
     * @Route("/company/new", name="new_company")
     */
    public function new(Request $request)
    {
        $company = new Company();

        $form = $this->createFormBuilder($company)
            ->add('name', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('address', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('siteUrl', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('phoneNumber', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label'=> 'Create','attr'=>array('class'=>'btn btn-primary mt-3')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $company = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($company);
            $entityManager->flush();

            return $this->redirectToRoute('company_list');
        }

        return $this->render('company/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/company/edit/{id}", name="edit_company", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $id)
    {
        $company = new Company();
        $company = $this->getDoctrine()->getRepository(Company::class)->find($id);

        $form = $this->createFormBuilder($company)
            ->add('name', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('address', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('siteUrl', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('phoneNumber', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label'=> 'Update','attr'=>array('class'=>'btn btn-primary mt-3')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('company_list');
        }

        return $this->render('company/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/company/{id}", name="company_show", requirements={"id"="\d+"})
     */
    public function show($id)
    {
        $company = $this->getDoctrine()->getRepository(Company::class)->find($id);

        return $this->render('company/show.html.twig', [
            'company' => $company,
        ]);
    }

    /**
     * @Route("/company/delete/{id}", name="company_delete", requirements={"id"="\d+"}, methods={"DELETE"})
     */
    public function delete(Request $request, $id)
    {
        $company = $this->getDoctrine()->
        getRepository(Company::class)
            ->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($company);
        $entityManager->flush();

        $response = new Response();
        $response->send();
    }
}
