const companies = document.getElementById('companies');

if (companies) {
    companies.addEventListener('click', (e)=>{
        if(e.target.className === 'btn btn-danger delete-article') {
            if(confirm('Are you sure?')) {
                const id = e.target.getAttribute('data-id');
                fetch(`/company/delete/${id}`, {
                    method: 'DELETE'
                }).then(res => window.location.reload())
            }
        }

    });
}

const resumes = document.getElementById('resumes');
if (resumes) {
    resumes.addEventListener('click', (e)=>{
        if(e.target.className === 'btn btn-danger delete-article') {
            if(confirm('Are you sure?')) {
                const id = e.target.getAttribute('data-id');
                fetch(`/resume/delete/${id}`, {
                    method: 'DELETE'
                }).then(res => window.location.reload())
            }
        }

    });
}

function filterByLikes() {
    $.ajax({
        type: "POST",
        url: "/resume/filter/likes",
        success: function(response) {
            $('#resumes').html(response);
        }
    });
}

function like_update(resumeId) {
    $.ajax({
        dataType: 'json',
        type: "POST",
        url: "/resume/like/"+resumeId,
        success: function(response) {
            $('#like-'+resumeId).text(response);
        }
    });
}

function dislike_update(resumeId) {
    $.ajax({
        dataType: 'json',
        type: "POST",
        url: "/resume/dislike/"+resumeId,
        success: function(response) {
            $('#dislike-'+resumeId).text(response);
        }
    });
}
